module Main (main) where

import Lib

import qualified TestVotes as TV
import Data.Either.Extra ( maybeToEither )

-- | Runs schulze on Votes in TestVotes and prints out the results
main :: IO ()
main = do
    either putStrLn print $ do
        candidates <- maybeToEither "Couldn't parse candidates" $ candidateList TV.candidates
        votes <- maybeToEither "Couldn't parse votes" $ rankingList candidates TV.votes
        return $ schulze candidates votes
module Lib
    ( schulze
    , CandidateList
    , candidateList
    , RankingList
    , rankingList
) where

import Data.List ( sortBy )
import Control.Monad ( guard )
import Data.Function ( on )
import Data.List.Extra ( anySame )
import Data.List.Match ( equalLength )


import Control.Applicative ( Applicative(liftA2) )

import Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map
import Data.STRef ( modifySTRef, newSTRef, readSTRef )
import Control.Monad.ST ( runST )


newtype CandidateList a = CandidateList [a]
    deriving Show

newtype CandidateRanking a = CandidateRanking [a]
    deriving Show

newtype RankingList a = RankingList [CandidateRanking a]

-- | Runs the schulze-method on a list of candidates and list of votes.
-- Returns the candidates sorted ascending by wins and their wins.
schulze :: Ord a => CandidateList a -> RankingList a -> [(a, Int)]
schulze cs rks = let
    d = pairwisePreference rks
    p = strongestPathStrength cs d
    wins = strongestPathStrengthsToWins cs p
    in wins
    

pairwisePreference :: Eq a => RankingList a -> a -> a -> Int
pairwisePreference (RankingList rks) x y = length $ do
    (CandidateRanking ranking) <- rks
    let onlyXY = filter (liftA2 (||) (==x) (==y)) ranking
    guard $ onlyXY == [x, y] -- check if x comes before y
    return ()
    
strongestPathStrength :: Ord a => CandidateList a -> (a -> a -> Int) -> Map (a, a) Int
strongestPathStrength (CandidateList cs) pp = runST $ do
    strongestPathMapVar <- newSTRef Map.empty
    let insertIntoMap = \k v -> modifySTRef strongestPathMapVar $ Map.insert k v
    sequence_ $ do
        candidateI <- cs
        candidateJ <- cs
        guard $ candidateI /= candidateJ
        return $ if (pp candidateI candidateJ) > (pp candidateJ candidateI)
            then insertIntoMap (candidateI, candidateJ) (pp candidateI candidateJ)
            else insertIntoMap (candidateI, candidateJ) 0
    sequence_ $ do
        candidateI <- cs
        candidateJ <- cs
        guard $ candidateI /= candidateJ
        candidateK <- cs
        guard $ candidateI /= candidateK
        guard $ candidateJ /= candidateK
        return $ do
            strongestPathMap <- readSTRef strongestPathMapVar
            maybe (error "Messed up in Floyd-Warshall") id $ do
                jk <- strongestPathMap Map.!? (candidateJ, candidateK)
                ji <- strongestPathMap Map.!? (candidateJ, candidateI)
                ik <- strongestPathMap Map.!? (candidateI, candidateK)
                let result = max jk (min ji ik)
                return $ do
                    insertIntoMap (candidateJ, candidateK) result
    readSTRef strongestPathMapVar

strongestPathStrengthsToWins :: Ord a => CandidateList a -> Map (a, a) Int -> [(a, Int)]
strongestPathStrengthsToWins (CandidateList cs) sps = sortBy (compare `on` snd) $ do
    candidateI <- cs
    let defeatedCount = length $ do
            candidateJ <- cs
            guard $ candidateI /= candidateJ
            guard . maybe False id $ do
                ij <- sps Map.!? (candidateI, candidateJ)
                ji <- sps Map.!? (candidateJ, candidateI)
                return $ ij > ji
    return (candidateI, defeatedCount)

candidateList :: Eq a => [a] -> Maybe (CandidateList a)
candidateList candidates = do
    guard . not . anySame $ candidates
    return $ CandidateList candidates

candidateRanking :: Eq a => CandidateList a -> [a] -> Maybe (CandidateRanking a)
candidateRanking (CandidateList cs) ranking = do
    guard . not . anySame $ ranking -- all elements are unique
    guard $ equalLength cs ranking -- ranking is as long as candidate list
    return $ CandidateRanking ranking

rankingList :: Eq a => CandidateList a -> [[a]] -> Maybe (RankingList a)
rankingList cs rks = do
    let rankings = candidateRanking cs <$> rks
    RankingList <$> sequence rankings
